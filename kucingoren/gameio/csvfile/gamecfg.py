"""
CSV game configuration dictionary module.
"""
from __future__ import annotations
import typing as _typing
from re import (
    compile as _rec
)
from . import (
    _generic_find as _generic_find
    , _serialize as _csvserialize
    , _ReadableFile as _ReadableFile
    , _StrBytes as _StrBytes
    , CSV as _CSV
    , load as _load
    , loads as _loads
    , dump as _dump
    , dumps as _dumps
)
from ...util.serializer import (
    serialize_f as _srl_f
    , serialize_f2p as _srl_f2p
)
from ...util.json_ import (
    get_json_module as __get_json
)
_json = __get_json()



_StrictDict = _typing.Dict[_typing.Pattern[str], _typing.Callable[[_typing.Any,],_typing.Any]]
"""
Parameters
----------
key: re.Pattern
    RegEx pattern to match up with the config key.
value: Callable[[Any], Any]
    Function with one parameter as config value input and return with serialized value.
"""
_StrictOptions = _typing.Union[bool, _StrictDict]


SERIALIZE_STRICT: _StrictDict = {
    _rec(r'game\.skills\.base\.[\d]+\.amount') : _srl_f2p
    , _rec(r'game\.skills\.heavy\.1\.amount') : _srl_f2p
    , _rec(r'game\.enemy\.zombieAttributes') : _srl_f2p
    , _rec(r'game\.enemy\.zombieAttributes\.boss\.meleeDamage\.damageIncreaseNightmare[\w]*') : _srl_f
    , _rec(r'game\.loot\.rollDropRates\.area\.consumable') : _srl_f2p
    , _rec(r'game\.loot\.claimStrongbox\.weaponAugTypePool\.damageOverTime') : _srl_f2p
    , _rec(r'game\.loot\.rollConsumableRates') : _srl_f2p
    , _rec(r'game\.loot\.claimStrongbox\.equipmentAugTypePool') : _srl_f2p
    , _rec(r'game\.liveOps\.gameModes\.[\d]+\.balance\.[\w]*[Vv]irus[\w]*') : _srl_f2p
    , _rec(r'game\.liveOps\.[\w]+\.[\d]+\.balance\.[\w]*[Ss]cale[\w]*') : _srl_f2p
    , _rec(r'game\.liveOps\.gameModes\.1\.balance\.ignoreArmorScalePerIncrease') : _srl_f
    , _rec(r'game\.elite\.[\w]*BlackKeyChance') : _srl_f2p
}
"""Default serialize strict `dict`."""


class GameConfigDict (_typing.Dict[str, _typing.Any]):
    """
    CSV game configuration dictionary.
    Inherits from `dict`.
    """
    @classmethod
    def from_csv (cls, inval: _CSV) -> "GameConfigDict":
        """Initiate a new instance from `CSV` object."""
        return cls( _deserialize(x[0], x[1]) for x in inval )

    def to_csv (self, strict: _StrictOptions|None = None) -> _CSV:
        """
        Serialize `self` to a `CSV` object.

        Parameters
        ----------
        strict: _StrictOptions|None
            Known keys will be pre-serialized
            before exported into return value.
            Default is `False`.
            - `True`:
                Set with default strict `dict`.
            - `False`:
                Disable strict serializing.
            - `dict`:
                User-defined strict dictionary.
        """
        if strict is None:
            strict = False
        elif strict is True:
            strict = SERIALIZE_STRICT
        it = (_serialize(*i, strict=strict) for i in self.items())
        return _CSV( it )

    def find (
        self
        , key: str|_typing.Any
        , default: _typing.Any = None
        , offset: int|None = None
        , multi_value: bool|None = None
    ) -> tuple[str,_typing.Any]|list[tuple[str,_typing.Any]]:
        """
        Get the item by given key.
        Return *default* if not found.

        Parameters
        ----------
        key: str|Any
            Search key.
        default: Any
            Default value to be returned if key is not found.
        offset: int|None
            Search item offset.
        multi_value: bool|None
            Return multiple values of given `key`, if there's duplication.
            Default is `False`.
        """
        return _generic_find( self.items(), key, default, offset, multi_value )


def dump (
    obj: GameConfigDict
    , fp: _ReadableFile
    , strict: _StrictOptions|None = None
) -> None:
    """
    Serialize *obj* as a CSV-formatted stream to file-like *fp*.

    Parameters
    ----------
    obj: GameConfigDict
        Input value.
    strict: _StrictOptions|None
        See `GameConfigDict.to_csv`.
    """
    return _dump( obj.to_csv(strict), fp )


def dumps (
    obj: GameConfigDict
    , strict: _StrictOptions|None = None
) -> str:
    """
    Serialize *obj* to a CSV-formatted `str`.

    Parameters
    ----------
    obj: GameConfigDict
        Input value.
    strict: _StrictOptions|None
        See `GameConfigDict.to_csv`.
    """
    return _dumps( obj.to_csv(strict) )


def load (
    fp: _ReadableFile
) -> GameConfigDict:
    """Deserialize file-like *fp* to a `GameConfigDict` object."""
    return GameConfigDict.from_csv( _load(fp, 2) )


def loads (
    inval: _StrBytes
) -> GameConfigDict:
    """Deserialize *inval* to a `GameConfigDict` object."""
    return GameConfigDict.from_csv( _loads(inval, 2) )


def _serialize (
    key: str
    , val: _typing.Any
    , strict: _StrictDict|_typing.Literal[False]|None = None
) -> tuple[str,str]:
    if strict is None:
        strict = SERIALIZE_STRICT
    if strict:
        for k,v in strict.items():
            if k.match( key ):
                val = v( val )
    return key, _csvserialize(val)


def _deserialize (
    key: _typing.Any
    , val: _typing.Any
) -> tuple[str,_typing.Any]:
    key = str(key)
    try:
        return key, _json.loads(val)
    except _json.JSONDecodeError:
        return key, val
