"""
DGDATA module.

Ported & reworked from decompiled SAS: Zombie Assault 4 flash file (sas4.swf).
"""
from __future__ import annotations
import typing as _typing
import abc as _abc
from collections import (
    deque as _deque
)
from io import (
    DEFAULT_BUFFER_SIZE as _DEFAULT_BUFFER_SIZE
    , SEEK_CUR as _SEEK_CUR
    , BytesIO as _BytesIO
)
from functools import (
    lru_cache as _cache
)
import numpy as _np
from ..util import (
    BufferLike as _BufferLike
)
from ..util.io_ import (
    get_fileobj_size as _get_fp_size
)



HEADER_SIZE: int = 14
"""DGDATA header size in bytes (DGDATAxxxxxxxx)."""


HEADER_MAGIC: bytes = b"DGDATA"
"""DGDATA header signature."""


CHECKSUM_SIZE: int = HEADER_SIZE - len(HEADER_MAGIC)
"""DGDATA checksum size in bytes."""


_BYTEHASH_TABLE = _np.array( (
    0x00000000, 0x09073096, 0x120E612C, 0x1B0951BA, 0xFF6DC419,
    0xF66AF48F, 0xED63A535, 0xE46495A3, 0xFEDB8832, 0xF7DCB8A4,
    0xECD5E91E, 0xE5D2D988, 0x01B64C2B, 0x08B17CBD, 0x13B82D07,
    0x1ABF1D91, 0xFDB71064, 0xF4B020F2, 0xEFB97148, 0xE6BE41DE,
    0x02DAD47D, 0x0BDDE4EB, 0x10D4B551, 0x19D385C7, 0x036C9856,
    0x0A6BA8C0, 0x1162F97A, 0x1865C9EC, 0xFC015C4F, 0xF5066CD9,
    0xEE0F3D63, 0xE7080DF5, 0xFB6E20C8, 0xF269105E, 0xE96041E4,
    0xE0677172, 0x0403E4D1, 0x0D04D447, 0x160D85FD, 0x1F0AB56B,
    0x05B5A8FA, 0x0CB2986C, 0x17BBC9D6, 0x1EBCF940, 0xFAD86CE3,
    0xF3DF5C75, 0xE8D60DCF, 0xE1D13D59, 0x06D930AC, 0x0FDE003A,
    0x14D75180, 0x1DD06116, 0xF9B4F4B5, 0xF0B3C423, 0xEBBA9599,
    0xE2BDA50F, 0xF802B89E, 0xF1058808, 0xEA0CD9B2, 0xE30BE924,
    0x076F7C87, 0x0E684C11, 0x15611DAB, 0x1C662D3D, 0xF6DC4190,
    0xFFDB7106, 0xE4D220BC, 0xEDD5102A, 0x09B18589, 0x00B6B51F,
    0x1BBFE4A5, 0x12B8D433, 0x0807C9A2, 0x0100F934, 0x1A09A88E,
    0x130E9818, 0xF76A0DBB, 0xFE6D3D2D, 0xE5646C97, 0xEC635C01,
    0x0B6B51F4, 0x026C6162, 0x196530D8, 0x1062004E, 0xF40695ED,
    0xFD01A57B, 0xE608F4C1, 0xEF0FC457, 0xF5B0D9C6, 0xFCB7E950,
    0xE7BEB8EA, 0xEEB9887C, 0x0ADD1DDF, 0x03DA2D49, 0x18D37CF3,
    0x11D44C65, 0x0DB26158, 0x04B551CE, 0x1FBC0074, 0x16BB30E2,
    0xF2DFA541, 0xFBD895D7, 0xE0D1C46D, 0xE9D6F4FB, 0xF369E96A,
    0xFA6ED9FC, 0xE1678846, 0xE860B8D0, 0x0C042D73, 0x05031DE5,
    0x1E0A4C5F, 0x170D7CC9, 0xF005713C, 0xF90241AA, 0xE20B1010,
    0xEB0C2086, 0x0F68B525, 0x066F85B3, 0x1D66D409, 0x1461E49F,
    0x0EDEF90E, 0x07D9C998, 0x1CD09822, 0x15D7A8B4, 0xF1B33D17,
    0xF8B40D81, 0xE3BD5C3B, 0xEABA6CAD, 0xEDB88320, 0xE4BFB3B6,
    0xFFB6E20C, 0xF6B1D29A, 0x12D54739, 0x1BD277AF, 0x00DB2615,
    0x09DC1683, 0x13630B12, 0x1A643B84, 0x016D6A3E, 0x086A5AA8,
    0xEC0ECF0B, 0xE509FF9D, 0xFE00AE27, 0xF7079EB1, 0x100F9344,
    0x1908A3D2, 0x0201F268, 0x0B06C2FE, 0xEF62575D, 0xE66567CB,
    0xFD6C3671, 0xF46B06E7, 0xEED41B76, 0xE7D32BE0, 0xFCDA7A5A,
    0xF5DD4ACC, 0x11B9DF6F, 0x18BEEFF9, 0x03B7BE43, 0x0AB08ED5,
    0x16D6A3E8, 0x1FD1937E, 0x04D8C2C4, 0x0DDFF252, 0xE9BB67F1,
    0xE0BC5767, 0xFBB506DD, 0xF2B2364B, 0xE80D2BDA, 0xE10A1B4C,
    0xFA034AF6, 0xF3047A60, 0x1760EFC3, 0x1E67DF55, 0x056E8EEF,
    0x0C69BE79, 0xEB61B38C, 0xE266831A, 0xF96FD2A0, 0xF068E236,
    0x140C7795, 0x1D0B4703, 0x060216B9, 0x0F05262F, 0x15BA3BBE,
    0x1CBD0B28, 0x07B45A92, 0x0EB36A04, 0xEAD7FFA7, 0xE3D0CF31,
    0xF8D99E8B, 0xF1DEAE1D, 0x1B64C2B0, 0x1263F226, 0x096AA39C,
    0x006D930A, 0xE40906A9, 0xED0E363F, 0xF6076785, 0xFF005713,
    0xE5BF4A82, 0xECB87A14, 0xF7B12BAE, 0xFEB61B38, 0x1AD28E9B,
    0x13D5BE0D, 0x08DCEFB7, 0x01DBDF21, 0xE6D3D2D4, 0xEFD4E242,
    0xF4DDB3F8, 0xFDDA836E, 0x19BE16CD, 0x10B9265B, 0x0BB077E1,
    0x02B74777, 0x18085AE6, 0x110F6A70, 0x0A063BCA, 0x03010B5C,
    0xE7659EFF, 0xEE62AE69, 0xF56BFFD3, 0xFC6CCF45, 0xE00AE278,
    0xE90DD2EE, 0xF2048354, 0xFB03B3C2, 0x1F672661, 0x166016F7,
    0x0D69474D, 0x046E77DB, 0x1ED16A4A, 0x17D65ADC, 0x0CDF0B66,
    0x05D83BF0, 0xE1BCAE53, 0xE8BB9EC5, 0xF3B2CF7F, 0xFAB5FFE9,
    0x1DBDF21C, 0x14BAC28A, 0x0FB39330, 0x06B4A3A6, 0xE2D03605,
    0xEBD70693, 0xF0DE5729, 0xF9D967BF, 0xE3667A2E, 0xEA614AB8,
    0xF1681B02, 0xF86F2B94, 0x1C0BBE37, 0x150C8EA1, 0x0E05DF1B,
    0x0702EF8D
), _np.uint32 )
"""Pre-computed ByteHash table for faster hashing."""


class DGDataEncodeError (Exception):
    """Failed to encode."""


class DGDataDecodeError (Exception):
    """Failed to decode."""


class DGDataHash ():
    """
    DGDATA hash algorithm.

    """
    @property
    def digest_size (self) -> int:
        return 4

    @property
    def block_size (self) -> int:
        return 1

    @property
    def name (self) -> str:
        return "dgdata"

    def __init__ (self, data: bytes|None = None) -> None:
        self._digest = _np.uint32( 0 )
        if data:
            self.update( data )

    def update (self, data: bytes) -> None:
        loc7 = self._digest
        for teh_byte in iter( data ):
            loc2 = teh_byte
            loc3 = (loc7 ^ loc2) & 255
            loc7 = ((loc7 >> 8) & 16777215) ^ _BYTEHASH_TABLE[loc3]
        self._digest = _np.uint32( loc7 )

    def digest (self) -> bytes:
        return self.hexdigest().encode('ascii')

    def hexdigest (self) -> str:
        #self._normalize()
        return format_checksum_hex( self._digest )

    def copy (self) -> "DGDataHash":
        cls = self.__class__
        retval = cls()
        retval._digest = self._digest.copy()
        return retval

    '''
    def _normalize (self) -> None:
        # From original SWF code.
        # Dunno why, since unsigned int never has negative value.
        loc7 = self._digest
        if (loc7 < 0):
            self._digest = _np.uint32( 4294967295 + loc7 + 1 )
    '''


'''
def _bytehash (num: int) -> _np.uint32:
    loc2 = _np.uint32( num )
    loc4 = 3988292384
    for _ in range( 8 ):
        loc2 = loc2.view( _np.int32 )
        if (loc2 & 1):
            loc2 >>= 1
            loc2 ^= loc4
        else:
            loc2 >>= 1
        loc2 = _np.uint32( loc2 )
    return loc2
def get_hash (data: bytes) -> _np.uint32:
    """
    Return raw content hash.

    Parameters
    ----------
    data: bytes
        Iterable bytes.
    """
    loc7 = _np.uint32( 0 )
    for loc6 in range( len(data) ):
        loc2 = data[loc6]
        loc3 = (loc7 ^ loc2) & 255
        loc7 = _np.uint32( ((loc7 >> 8) & 16777215) ^ _bytehash(loc3) )
    if (loc7 < 0):
        loc7 = _np.uint32( 4294967295 + loc7 + 1 )
    return loc7
'''


class _BaseProcessor (_abc.ABC):
    @property
    def size (self) -> int:
        """Total size of data to be processed (Negatives = No limit)."""
        return self._data_size

    @property
    def cursor (self) -> int:
        """Data cursor."""
        return self._data_cursor

    @property
    def remaining_size (self) -> int|None:
        """Remaining size of data left to be processed."""
        size = self.size
        if size < 0:
            return None
        return max( 0, size - self.cursor )

    @property
    @_abc.abstractmethod
    def checksum (self) -> bytes|None:
        """Data checksum."""

    @property
    @_abc.abstractmethod
    def hexchecksum (self) -> str|None:
        """Data checksum in hex string."""

    def __init__ (self, size: int|None = None) -> None:
        """
        Parameters
        ----------
        size: int
            Total size of data.
        """
        self._hash = DGDataHash()
        self._data_cursor: int = 0
        if size is None:
            size = -1
        self._data_size: int = size

    @_abc.abstractmethod
    def digest (self, data: bytes) -> bytes:
        ...

    def __len__ (self) -> int:
        return self.size

    def _processor_digest (
        self
        , data: bytes
        , func: _typing.Callable[[int, int], int]
        , is_encode: bool
    ) -> bytes:
        new_cursor = self._data_cursor + len( data )
        total_size = self._data_size
        if total_size > -1 and new_cursor > total_size:
            raise ValueError( "Data cursor is exceeded the data size" )
        retval = bytes(
            func( v,i )
            for i,v
            in enumerate( iter(data), self._data_cursor )
        )
        self._hash.update( data if is_encode else retval )
        self._data_cursor = new_cursor
        return retval


class Encoder (_BaseProcessor):
    """
    Incremental DGDATA encoder.

    """
    @property
    def checksum (self) -> bytes:
        return self._hash.digest()

    @property
    def hexchecksum (self) -> str:
        return self._hash.hexdigest()

    def digest (self, data: bytes) -> bytes:
        """Encode given bytes and move the cursor forward."""
        return self._processor_digest( data, encode_byte, True )


class Decoder (_BaseProcessor):
    """
    Incremental DGDATA decoder.

    """
    @property
    def checksum (self) -> bytes|None:
        return self._data_checksum

    @property
    def hexchecksum (self) -> str|None:
        chk = self._data_checksum
        if chk is None:
            return None
        return chk.decode( "ascii" )

    def __init__ (self, size: int|None = None, checksum: bytes|int|str|None = None) -> None:
        """
        Parameters
        ----------
        size: int
            Total size of the data.
        checksum: bytes|int|str|None
            Data checksum to be checked for.
        """
        super(Decoder, self).__init__( size )
        if isinstance( checksum, str ):
            checksum = int( checksum, 16 )
        if isinstance( checksum, int ):
            checksum = checksum.to_bytes( 4, 'little' )
        self._data_checksum = checksum
        self._checksum_valid_check()

    def digest (self, data: bytes) -> bytes:
        """Decode given bytes and move the cursor forward."""
        return self._processor_digest( data, decode_byte, False )

    def verify (self) -> bool:
        """Verify the data checksum."""
        if not self._checksum_valid_check():
            raise ValueError( "Decoder has no checksum" )
        return self._hash.digest() == self._data_checksum

    def _checksum_valid_check (self) -> bool:
        if self._data_checksum is None:
            return False
        chk_len = len( self._data_checksum )
        if chk_len != CHECKSUM_SIZE:
            raise ValueError(
                f"Expected checksum bytes length to be {CHECKSUM_SIZE}, got %d"
                , chk_len
            )
        # check hexstring format.
        int( self._data_checksum, base=16 )
        return True


def iter_encode_fromfile (
    fp: _typing.BinaryIO
    , data_size: int|None = None
    , buffer_size: int = _DEFAULT_BUFFER_SIZE
) -> _typing.Iterator[tuple[bytes, tuple[int,int]|None]]:
    """
    Read & encode the given file object by per chunks.

    Parameters
    ----------
    fp: file object
        A readable file object.
    data_size: int|None
        Total size of data bytes.
        - `None`:
            Try to guess automatically.
            Otherwise, set to -1.
        - Negatives:
            Read until EOS.

    buffer_size: int
        Read buffer length.

    Returns
    ----------
    Each yield return tuple of:
    - Encoded chunk bytes, and
    - Tuple of seek offset & its type to be manually set by the file writer, if any.
    """
    try:
        data_size = _get_fp_size( fp )
        encoder = Encoder( data_size )
        # Yield the magic header.
        yield (HEADER_MAGIC, None)
        yield (b'\x00' * CHECKSUM_SIZE, None)
        # Yield the encoded content.
        for chunk in _iter_processor_read( fp, buffer_size, encoder ):
            yield (chunk, None)
        # Yield the checksum.
        yield (
            encoder.hexchecksum.encode( "ascii" )
            , ( (-encoder.cursor) - CHECKSUM_SIZE, _SEEK_CUR )
        )
    except BaseException as exc:
        raise DGDataEncodeError( exc )


def iter_encode_frombuffer (
    buffer: _BufferLike
    , *args
    , **kwargs
) -> _typing.Iterator[bytes]:
    """
    Encode the given readable buffer object by per chunks.

    Parameters
    ----------
    buffer: util.BufferLike
        A readable buffer object.
    *args, **kwargs:
        See `iter_encode_fromfile` for available arguments.

    Returns
    ----------
    See `iter_encode_fromfile`.
    """
    ie = iter_encode_fromfile( _BytesIO(buffer), *args, **kwargs )
    # Ignore the blank header.
    next( ie ), next( ie )
    # Magic header.
    yield HEADER_MAGIC
    retval: _typing.Deque[bytes] = _deque()
    append = retval.append
    for chunk, seek_params in ie:
        # Checksum.
        if seek_params:
            yield chunk
            continue
        append( chunk )
    # Data.
    yield from retval


def encode_tofile (
    buffer: _BufferLike
    , fp: _typing.BinaryIO
    , *args
    , **kwargs
) -> None:
    """
    Read & encode the given buffer-like object,
    and then write it into file object.

    Parameters
    ----------
    buffer: util.BufferLike
        A readable buffer object.
    fp: file object
        A writable binary file object.
    *args, **kwargs
        See `iter_encode_fromfile` for available arguments.
    """
    for chunk in iter_encode_frombuffer(buffer, *args, **kwargs):
        fp.write( chunk )


def encode_readwrite_helper (
    fp_output: _typing.BinaryIO
    , fp_input: _typing.BinaryIO
    , *args
    , **kwargs
) -> None:
    """
    Read & encode the input file object, and then
    write it into output file object.

    The output file object must be seekable due to offset changes multiple times.

    Parameters
    ----------
    fp_output: file object
        A writable, seekable binary file object.
    fp_input: file object
        A readable binary file object.
    *args, **kwargs
        See `iter_encode_fromfile` for available arguments.
    """
    for chunk, seek_params in iter_encode_fromfile(fp_input, *args, **kwargs):
        if seek_params is not None:
            fp_output.seek( *seek_params )
        fp_output.write( chunk )


def iter_decode_fromfile (
    fp: _typing.BinaryIO
    , data_size: int|None = None
    , buffer_size: int = _DEFAULT_BUFFER_SIZE
    , verify: bool = False
) -> _typing.Iterator[bytes]:
    """
    Read & decode the given file object by per chunks.

    Parameters
    ----------
    fp: file object
        A readable binary file object.
    data_size: int|None
        Total size of data bytes.
        - `None`:
            Try to guess automatically.
            Otherwise, set to -1.
        - Negatives:
            Read until EOS.

    buffer_size: int
        Read buffer length.
    verify: bool
        Set `True` to verify the data checksum at the end of decoding.
        Default is `False`.
    """
    try:
        data_size = _get_fp_size( fp ) if data_size is None else data_size
        # Get the checksum.
        data_checksum = None
        buf = fp.read( HEADER_SIZE )
        try:
            data_checksum = get_checksum( buf )
        except ValueError:
            if verify:
                raise
        else:
            if data_size is not None:
                data_size -= HEADER_SIZE
                if data_size < 0:
                    raise ValueError(
                        f"Expected header bytes length to be {HEADER_SIZE}, got %d"
                        , HEADER_SIZE+data_size
                    )
        decoder = Decoder( data_size, data_checksum )
        # Failed to read the header, but it return bytes anyway...
        if not data_checksum and buf:
            yield decoder.digest( buf )
        for chunk in _iter_processor_read( fp, buffer_size, decoder ):
            yield chunk
        if verify:
            if not decoder.verify():
                raise ValueError(
                    f"Expected checksum to be {data_checksum.decode('utf8')}, got %s"
                    , decoder._hash.hexdigest()
                )
    except BaseException as exc:
        raise DGDataDecodeError( exc )


def iter_decode_frombuffer (
    buffer: _BufferLike
    , *args, **kwargs
) -> _typing.Iterator[bytes]:
    """
    Decode the given readable buffer object by per chunks.

    Parameters
    ----------
    buffer: util.BufferLike
        A readable buffer object.
    *args, **kwargs:
        See `iter_decode_fromfile` for available arguments.

    Returns
    ----------
    See `iter_decode_fromfile`.
    """
    return iter_decode_fromfile( _BytesIO(buffer), *args, **kwargs )


def decode_tofile (
    buffer: _BufferLike
    , fp: _typing.BinaryIO
    , *args
    , **kwargs
) -> None:
    """
    Read & decode the given buffer-like object,
    and then write it into file object.

    Parameters
    ----------
    buffer: util.BufferLike
        A readable buffer object.
    fp: file object
        A writable binary file object.
    *args, **kwargs
        See `iter_decode_fromfile` for available arguments.
    """
    for chunk in iter_decode_frombuffer(buffer, *args, **kwargs):
        fp.write( chunk )


def format_checksum_hex (
    checksum: _typing.SupportsInt
) -> str:
    """Format checksum to hex string."""
    return f'{checksum:0{CHECKSUM_SIZE:d}x}'


def get_checksum (
    data: bytes
) -> bytes:
    """Parse the header and return the checksum value."""
    retval: bytes = data[:HEADER_SIZE]
    if not retval.startswith( HEADER_MAGIC ):
        raise ValueError( "Header is not found" )
    return retval[len(HEADER_MAGIC):]


@_cache( maxsize=_DEFAULT_BUFFER_SIZE )
def encode_byte (
    v: int
    , cursor: int = 0
) -> int:
    """Encode a single byte."""
    v += cursor % 6
    v += 21
    return int(_np.uint8( v ))


@_cache( maxsize=_DEFAULT_BUFFER_SIZE )
def decode_byte (
    v: int
    , cursor: int = 0
) -> int:
    """Decode a single byte."""
    v -= 21
    v -= cursor % 6
    return int(_np.uint8( v ))


def _iter_processor_read (
    fp: _typing.IO[bytes]
    , buffer_size: int
    , processor: _BaseProcessor
) -> _typing.Iterator[bytes]:
    proc_digest = processor.digest
    io_read = fp.read
    read = (lambda: io_read(buffer_size))
    if processor.remaining_size is not None:
        read = (lambda: io_read(min(buffer_size, processor.remaining_size)))
    while chunk := read():
        yield proc_digest( chunk )
        remaining = processor.remaining_size
        if remaining is not None and remaining <= 0:
            break
