"""
ORJSON module loader.
"""
from __future__ import annotations
import typing as _typing
from ...util.io_ import (
    is_binary_filelike as _is_binary_filelike
)
from orjson import *
from orjson import (
    __all__ as _orjson__all__
    , dumps as _orig_dumps
)
__all__ = _orjson__all__ + (
    "dump"
    , "load"
)



def _default_dumps (
    __obj: _typing.Any
    , default: _typing.Callable[[_typing.Any], _typing.Any]|None = None
    , option: int|None = None
) -> bytes:
    if option is None:
        option = OPT_SERIALIZE_NUMPY
    return _orig_dumps( __obj, default, option )
_default_dumps.__doc__ = _orig_dumps


def dumps (
    __obj: _typing.Any
    , default: _typing.Callable[[_typing.Any], _typing.Any]|None = None
    , option: int|None = None
) -> str:
    return _default_dumps( __obj, default, option ).decode( "utf8" )
dumps.__doc__ = _orig_dumps


def dump (
    __obj: _typing.Any
    , fp: _typing.IO[_typing.Any]
    , default: _typing.Callable[[_typing.Any], _typing.Any]|None = None
    , option: int|None = None
) -> None:
    if _is_binary_filelike( fp ):
        fp.write( _default_dumps(__obj,default,option) )
    else:
        fp.write( dumps(__obj,default,option) )


def load (
    fp: _typing.IO[_typing.Any]
) -> _typing.Any:
    return loads( fp.read() )
