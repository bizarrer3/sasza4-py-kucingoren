"""
Common utility module.
"""
from __future__ import annotations
import typing as _typing
from types import (
    FunctionType as _UserFunc
    , MethodType as _UserMethod
    , BuiltinFunctionType as _CPPFunc
    , BuiltinMethodType as _CPPMethod
    , LambdaType as _Lambda
)
from array import (
    array as _array
)
from mmap import (
    mmap as _mmap
)
from itertools import (
    chain as _chain
)
from functools import (
    lru_cache as _cache
)
import typing_extensions as _typingext
from numpy import (
    generic as _np_generic
    , ndarray as _np_ndarray
)
from numpy._typing._generic_alias import (
    _GENERIC_ALIAS_TYPE as _np_GENERIC_ALIAS_TYPE
)


_T = _typing.TypeVar( "_T" )
_T_co = _typing.TypeVar( "_T_co", covariant=True )
_T_Collection = _typing.TypeVar( "_T_Collection", bound=_typing.Collection )
_GENERIC_ALIAS_TYPES = tuple({
    *_np_GENERIC_ALIAS_TYPE
    , _typing._GenericAlias
    , getattr(_typingext, "_typing_GenericAlias", _typing._GenericAlias)
    , getattr(_typingext, "_BaseGenericAlias", _typing._GenericAlias)
    , _typingext.ParamSpecArgs
    , _typingext.ParamSpecKwargs
})
_ShapedArray_Size = _typing.Union[_typing.SupportsInt, str]
_T_ShapedArray_Size = _typing.TypeVar( "_T_ShapedArray_Size", bound=_ShapedArray_Size )


GenericAliases: _typingext.TypeAlias = _typing.Union.__getitem__( _GENERIC_ALIAS_TYPES )
"""ABCs for generic alias types."""


# FIXME: Copied from Numpy
# There is currently no exhaustive way to type the buffer protocol,
# as it is implemented exclusively in the C API (python/typing#593)
BufferLike = _typing.Union[
    bytes,
    bytearray,
    memoryview,
    _array,
    _mmap,
    _np_ndarray,
    _np_generic,
]
"""ABCs for buffer types."""


ShapedArray = _typingext.Annotated
"""Annotated type of with argument array size & element type."""


def get_shapedarray_args (
    tp: _typing.Type[_typing.Any]
) -> tuple[_typing.Type[_typing.Any], _ShapedArray_Size, _typing.Type[_typing.Any]]|None:
    """
    Return tuple of parameters in `ShapedArray`-alike generics.
    Otherwise, return `None`.
    """
    if not is_subclass_or_instance( _typingext.get_origin(tp), _typingext.Annotated ):
        return None
    cls, el_size, el_cls = _typingext.get_args( tp )
    return cls, el_size, el_cls


def cacheable (
    func: _T = None
    , *args
    , **kwargs
) -> _T:
    """
    Similar to `functools.lru_cache`
    but won't force cache the result
    if one of parameters isn't hashable.

    Parameters
    ----------
    func: typing.Callable
        Any callable object.
    *args, **kwargs:
        See `functools.lru_cache` for available keyword arguments.
    """
    def __wrapper (func: _T, *args, **kwargs):
        @_cache( *args, **kwargs )
        def cached_func (*args, **kwargs):
            return func( *args, **kwargs )
        def wrapper (*args, **kwargs):
            try:
                return cached_func( *args, **kwargs )
            except TypeError as exc:
                if "unhashable type:" in str(exc):
                    return func( *args, **kwargs )
                raise exc
        wrapper.cached = cached_func
        return wrapper
    # Called as @cacheable()? return the wrapper func.
    if func is None:
        return __wrapper
    return __wrapper( func, *args, **kwargs )


def is_subclass (
    v: _typing.Any
    , *args
    , **kwargs
) -> bool:
    """Similar to `issubclass` but it will cast to type object automatically."""
    v = v if isinstance( v, type ) else type( v )
    return issubclass( v, *args, **kwargs )


def is_subclass_or_instance (
    v: _typing.Any
    , *args
    , **kwargs
) -> bool:
    """
    Similar to `issubclass` and `isinstance`
    but it use the right function automatically,
    depend on type of `v`.
    """
    if isinstance( v, type ):
        return issubclass( v, *args, **kwargs )
    return isinstance( v, *args, **kwargs )


def is_function (
    v: _typing.Any
) -> bool:
    """Return `True` if given object is a function`."""
    return is_subclass_or_instance( v, (_Lambda, _UserFunc, _CPPFunc,) )


def is_method (
    v: _typing.Any
) -> bool:
    """Return `True` if given object is an instance method`."""
    return is_subclass_or_instance( v, (_UserMethod, _CPPMethod,) )


def is_namedtuple (
    v: _typing.Any
) -> bool:
    """Return `True` if given object is a `NamedTuple`."""
    return (
        is_subclass_or_instance(v, _typing.Sequence)
        and is_subclass_or_instance(v, tuple)
        and hasattr(v, "_make")
    )


@cacheable
def resolve_generic_origin (
    tp: GenericAliases
) -> _typing.Type[_typing.Any]:
    """Resolve the origin of deep-nested generics."""
    if isinstance( tp, _GENERIC_ALIAS_TYPES ):
        return resolve_generic_origin( tp.__origin__ )
    if tpo := _typingext.get_origin( tp ):
        return resolve_generic_origin( tpo )
    return tp


def copy_attr (
    outval: _T
    , inval: _typing.Any
    , func: _typing.Callable[[_typing.Any],_typing.Any]|None = None
) -> _T:
    """
    Perform `setattr` from `inval` into `outval` within same attributes.
    `AttributeError` errors will be ignored.
    Return `outval`.

    `func` is a function to be called before set the attribute.
    It has only one argument which is attribute value and returns it.
    """
    if func is None:
        func = (lambda x:x)
    inval_slots: _typing.Iterator[str] = (
        str(y)
        for x in type(inval).mro()
        for y in getattr(x, "__slots__", tuple())
    )
    inval_dict: _typing.Iterator[str] = (
        str(y)
        for y in getattr(inval, "__dict__", {})
    )
    for varname in set( _chain.from_iterable((inval_slots, inval_dict,)) ):
        try:
            setattr( outval, varname, func(getattr(inval, varname)) )
        except AttributeError:
            pass
    return outval
