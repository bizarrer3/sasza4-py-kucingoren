"""
ZIP file utility module.
"""
from __future__ import annotations
import typing as _typing
import struct as _struct
from collections import (
    deque as _deque
)
from copy import (
    copy as _shallowcopy
)
from os import (
    PathLike as _PathLike
)
from zipfile import (
    _FH_SIGNATURE
    , _FH_FILENAME_LENGTH
    , _FH_EXTRA_FIELD_LENGTH
    , _DD_SIGNATURE
    , structFileHeader as _structFileHeader
    , stringFileHeader as _stringFileHeader
    , sizeFileHeader as _sizeFileHeader
    , _SharedFile
    , BadZipFile as _BadZipFile
    , LargeZipFile as _LargeZipFile
    , ZipInfo as _ZipInfo
    , ZipFile as _ZipFile
)
from .. import (
    copy_attr as _copyattr
)
from ..io_ import (
    iter_fileobj_read as _iread
    , is_filelike as _is_filelike
)



# Type hints.
_DateTuple = _typing.Tuple[int, int, int, int, int, int]
_ReadWriteMode = _typing.Literal["r", "w"]
_ZipFileReadMode = _typing.Literal["r"]
_ZipFileWriteMode = _typing.Literal["w", "x", "a"]
_ZipFileMode = _typing.Union[_ZipFileReadMode, _ZipFileWriteMode]
_StrPath = _typing.Union[str, _PathLike]
_Compressor = _typing.IO[bytes]
_CompressorFactory = _typing.Callable[[int,_typing.Union[int,None]], _Compressor]
_Decompressor = _typing.IO[bytes]
_DecompressorFactory = _typing.Callable[[int], _Decompressor]
_Decrypter = _typing.Callable[[bytes], bytes]
_OptionalPassword = _typing.Union[bytes, _typing.Literal[False], None]
# General purpose bit flags
# Zip Appnote: 4.4.4 general purpose bit flag: (2 bytes)
_MASK_ENCRYPTED = 1 << 0
# Bits 1 and 2 have different meanings depending on the compression used.
_MASK_COMPRESS_OPTION_1 = 1 << 1
# _MASK_COMPRESS_OPTION_2 = 1 << 2
# _MASK_USE_DATA_DESCRIPTOR: If set, crc-32, compressed size and uncompressed
# size are zero in the local header and the real values are written in the data
# descriptor immediately following the compressed data.
_MASK_USE_DATA_DESCRIPTOR = 1 << 3
# Data structs.
_STRUCT_DATA_DESCRIPTOR = '<LLLL'
_SIZE_DATA_DESCRIPTOR = _struct.calcsize( _STRUCT_DATA_DESCRIPTOR )
_STRUCT_64_DATA_DESCRIPTOR = '<LLQQ'
_SIZE_64_DATA_DESCRIPTOR = _struct.calcsize( _STRUCT_64_DATA_DESCRIPTOR )


def _open_read_shared (
    zfile: _ZipFile
    , pos: int
) -> _typing.IO[bytes]:
    """Open read-only `zipfile._SharedFile` instance from given `zfile` object."""
    zfile._fileRefCnt += 1
    return _SharedFile(
        zfile.fp
        , pos
        , zfile._fpclose
        , zfile._lock
        , lambda: zfile._writing
    )


def _read_fileinfo_header (
    fp: _typing.IO[bytes]
) -> bytes:
    """Return the local file info bytes from given `fp` file-like object."""
    retval: _deque[int] = _deque()
    extend = retval.extend
    fheader_raw: bytes = fp.read( _sizeFileHeader )
    if len( fheader_raw ) != _sizeFileHeader:
        raise _BadZipFile( "Truncated file header" )
    extend( fheader_raw )
    fheader: tuple[_typing.Any,...] = _struct.unpack( _structFileHeader, fheader_raw )
    if fheader[_FH_SIGNATURE] != _stringFileHeader:
        raise _BadZipFile( "Bad magic number for file header" )
    extend( fp.read(fheader[_FH_FILENAME_LENGTH]) )
    if fheader[_FH_EXTRA_FIELD_LENGTH]:
        extend( fp.read(fheader[_FH_EXTRA_FIELD_LENGTH]) )
    return bytes( retval )


def _get_fileheader_editor (
    zfile: _ZipFile
) -> _typing.Callable[[bytes],bytes]:
    """Return the local file info header editor method."""
    return getattr(
        zfile
        , "edit_local_fileheader"
        , None
    ) or (lambda x:x)


def get_data_descriptor (
    zinfo: _ZipInfo
    , zip64: bool
) -> bytes:
    """Return the file info's data descriptor `bytes`."""
    return _struct.pack(
        _STRUCT_64_DATA_DESCRIPTOR if zip64 else _STRUCT_DATA_DESCRIPTOR
        , _DD_SIGNATURE
        , zinfo.CRC
        , zinfo.compress_size
        , zinfo.file_size
)


def rebuild (
    in_zfile: _ZipFile
    , out_zfile_or_path: _ZipFile|_StrPath|_typing.IO[bytes]
    , mode: _ZipFileWriteMode|None = None
) -> _ZipFile:
    """
    Rebuild the zip file into given `out_zfile_or_path` object.

    Parameters
    ----------
    in_zfile: zipfile.ZipFile
        Source zip file.
    out_zfile_or_path: zipfile.ZipFile|str|os.PathLike|typing.IO[bytes]
        Target zip file.
    mode: _ZipFileWriteMode|None
        Target open mode.
        Default is "x".
    """
    if mode is None:
        mode = "x"
    if not isinstance( in_zfile, _ZipFile ):
        raise TypeError( "in_zfile: expected ZipFile, got %s" % type(in_zfile).__name__ )
    if mode not in ('w', 'x', 'a'):
        raise ValueError("mode: ZipFile requires mode 'w', 'x', or 'a'")
    if isinstance( out_zfile_or_path, _ZipFile ):
        out_zfile: _ZipFile = out_zfile_or_path
    elif isinstance( out_zfile_or_path, (str, _PathLike,) ):
        out_zfile = (type(in_zfile))( str(out_zfile_or_path), mode )
    elif _is_filelike( out_zfile_or_path ):
        out_zfile = (type(in_zfile))( out_zfile_or_path, mode )
    else:
        raise TypeError(
            "out_zfile_or_path: expected ZipFile or str or file-like, got %s"
            % type(out_zfile_or_path).__name__
        )
    in_io = _open_read_shared( in_zfile, 0 )
    in_io_startpos: int|None = None
    try:
        in_io_startpos = in_zfile.fp.tell()
        info_append = out_zfile.filelist.append
        info_dictset = out_zfile.NameToInfo.__setitem__
        def _import (x: _ZipInfo) -> _ZipInfo:
            x = _copyattr( (type(x))(), x, _shallowcopy )
            info_append( x )
            info_dictset( x.filename, x )
            return x
        in_io_seek = in_io.seek
        out_io_tell = out_zfile.fp.tell
        out_io_write = out_zfile.fp.write
        fh_editor = _get_fileheader_editor( in_zfile )
        with out_zfile._lock:
            # Reset.
            out_zfile.fp.seek( 0 )
            out_zfile._didModify = True
            out_zfile.filelist.clear()
            out_zfile.NameToInfo.clear()
            # Copying...
            for zinfo in in_zfile.filelist:
                zinfo = _import( zinfo )
                in_offset, zinfo.header_offset = zinfo.header_offset, out_io_tell()
                # Prepare the source reader.
                in_io_seek( in_offset )
                _read_fileinfo_header( in_io )
                zip64: bool = False
                try:
                    zinfo_header = fh_editor( zinfo.FileHeader(zip64) )
                except _LargeZipFile:
                    zip64 = True
                    zinfo_header = fh_editor( zinfo.FileHeader(zip64) )
                # Write header.
                out_io_write( zinfo_header )
                # Copy content & data descriptor.
                read_total = zinfo.compress_size
                if zinfo.flag_bits & _MASK_USE_DATA_DESCRIPTOR:
                    read_total += _SIZE_64_DATA_DESCRIPTOR if zip64 else _SIZE_DATA_DESCRIPTOR
                for chunk in _iread( in_io, read_total ):
                    out_io_write( chunk )
            # New central dir offset and truncate the file.
            out_zfile.start_dir = out_io_tell()
            out_zfile.fp.truncate()
    finally:
        if in_io_startpos is not None:
            in_io.seek( in_io_startpos )
        in_io.close()
    return out_zfile
