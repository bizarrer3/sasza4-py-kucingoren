"""
Numpy utility module.
"""
from __future__ import annotations
import typing as _typing
from numpy import (
    uint as _d_uint
    , empty as _empty
    , prod as _prod
)
from numpy._typing import (
    _ShapeLike
    , _Shape
    , _DTypeLike
    , _DType
)
from .. import (
    cacheable as _cacheable
)



@_cacheable
def resolve_shape (
    dtype: _DTypeLike
) -> _Shape:
    """
    Return the true shape of given `numpy.dtype`-alike,
    especially for deep-nested scalar dtypes,
    by using `numpy.empty` with 1-D shape.
    """
    return _empty( 0, dtype=dtype ).shape[1:]


@_cacheable
def resolve_base (
    dtype: _DType
) -> _DType:
    """
    Return the base of given `numpy.dtype`,
    especially for deep-nested dtypes.
    """
    base = dtype.base
    if (
        base is dtype
        or not base.subdtype
    ):
        return base
    return resolve_base( base )


def shape_size (
    shape: _ShapeLike|_typing.SupportsInt
) -> int:
    """Return the number of elements in given shape."""
    if not shape:
        return 0
    return _prod( shape, dtype=_d_uint )
