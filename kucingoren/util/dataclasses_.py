"""
Dataclasses utility module.
"""
from __future__ import annotations
import typing as _typing
from abc import (
    ABC as _ABC
)
from dataclasses import (
    Field as _Field
    , is_dataclass as _is_dataclass
    , fields as _fields
    , asdict as _asdict
)
import typing_extensions as _typingext
from . import (
    _T as _T
    , is_namedtuple as _is_namedtuple
    , cacheable as _cacheable
)



class ABCDictDataclass (_ABC):
    """
    Abstract class for any dataclasses load-able via a `dict`.

    """
    @staticmethod
    def _format_kwargs (
        t_cls: _typing.Type[_typing.Any]
        , inval: dict[str, _typing.Any]
    ) -> dict[str, _typing.Any]:
        cls_types = _typingext.get_type_hints( t_cls )
        cls_fields = dict(
            (x.name, cls_types[x.name])
            for x
            in fields(t_cls)
        )
        for k,v in inval.items():
            if k not in cls_fields:
                continue
            t_field = cls_fields[k]
            field_constructor: _typing.Callable[[_typing.Any],_typing.Any] = t_field
            # TODO: Find an efficient way to clear up generic parameters.
            t_clear: _typing.Type[_typing.Any] = t_field
            clear_origin, clear_args = _typingext.get_origin( t_clear ), _typingext.get_args( t_clear )
            if clear_args:
                t_clear = clear_origin or t_clear
            if isinstance( v, t_clear ):
                continue
            # NamedTuple is, uh, unique.
            # Requires to use `_make` if `v` is a collection.
            if _is_namedtuple( t_field ):
                def _f (x: _typing.Any) -> _typing.NamedTuple:
                    try:
                        return t_field( x )
                    except TypeError:
                        return t_field._make( x )
                field_constructor = _f
            if _is_dataclass( t_field ) and isinstance( v, dict ):
                if hasattr( t_field, "from_dict" ):
                    inval[k] = t_field.from_dict( v )
                else:
                    inval[k] = t_field( **v )
                continue
            inval[k] = field_constructor( v )
        return inval

    @classmethod
    def from_dict (cls: _typing.Type[_T], inval: dict[str, _typing.Any]) -> _T:
        """Initiate a new instance from `dict` object."""
        return cls( **cls._format_kwargs(cls, inval) )

    def to_dict (self) -> dict[str, _typing.Any]:
        """Return as `dict` instance."""
        return _asdict( self )


@_cacheable
def fields (
    class_or_instance: _typing.Any
) -> tuple[_Field, ...]:
    """
    Same as `dataclasses.fields`
    but the return value is memoized, if `class_or_instance` is hashable.
    """
    return _fields( class_or_instance )
