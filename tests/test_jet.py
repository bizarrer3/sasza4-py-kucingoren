from os import remove
from pathlib import Path, PurePosixPath
from shutil import copyfileobj, copy
from hashlib import sha256
import pytest
from zipfile import ZIP_STORED, ZIP_DEFLATED, BadZipfile
from kucingoren.gameio import jetfile
from kucingoren.util.zipfile_ import _MASK_ENCRYPTED, rebuild
from . import TEMP_DIR, fp_sha256



JET_FILEPATH = Path( "./tests/resources/generic.jet" )
JET_SAMPLES = {
    ZIP_STORED: {
        "sha256": "39711c32140a0bbbaa894e4cf01f56e205c9d85913937ad3a528b7e34f7e65a5"
        , "files": (
            PurePosixPath( "cfg.csv" )
            , PurePosixPath( "subdir/cfg.csv" )
        )
    }
    , ZIP_DEFLATED: {
        "sha256": "f88b2b69973ac5ea80c4b9f84f8480507853331fddb1896256d8ddac27f9810b"
        , "files": (
            PurePosixPath( "lang.csv" )
            , PurePosixPath( "subdir/lang.csv" )
        )
    }
}


def _read (inval: jetfile.JetFile, compress_type, hash, filepath):
    filepath = str( filepath )
    info = inval.getinfo( filepath )
    assert info.compress_type == compress_type
    if hash:
        with inval.open( info ) as fp:
            assert fp_sha256( fp ) == hash


def _read_files (inval: jetfile.JetFile):
    for compress_type, v in JET_SAMPLES.items():
        for filepath in v["files"]:
            _read( inval, compress_type, v["sha256"], filepath )


def test_open_valid ():
    with jetfile.JetFile(JET_FILEPATH) as in_jet:
        _read_files( in_jet )


def test_open_invalid ():
    with pytest.raises( OSError ):
        jetfile.JetFile( "./tests/resources/nonexistent.jet" )
    with pytest.raises( BadZipfile ):
        jetfile.JetFile( "./tests/resources/valid.dgdata" )


def test_write_exclusive ():
    out_path = (TEMP_DIR / "test_write_exclusive.jet" )
    if out_path.exists():
        remove( out_path )
    with jetfile.JetFile( JET_FILEPATH ) as in_jet:
        with jetfile.JetFile( out_path, mode="x" ) as out_jet:
            for info in in_jet.infolist():
                copyfileobj( in_jet.open(info), out_jet.open(info, mode="w"), 4096 )
            _read_files( out_jet )
            assert out_jet._fileRefCnt == 1
        assert in_jet._fileRefCnt == 1
    with out_path.open( "rb" ) as fp:
        assert fp_sha256( fp ) == "6a24bdf8d3ffcdacd8b4f7250d468fbf22f7cbf4d0113e01b2c776ca928ec5c7"


def _write (inval: jetfile.JetFile, newfile, compress_type, hash, arcname=None, pwd=None):
    in_newfile = Path( newfile )
    in_arcname = arcname or in_newfile.name
    inval.write( in_newfile, in_arcname, compress_type, 5
        , allow_duplicate=False, allow_overwrite=True
        , pwd=pwd
    )
    _read( inval, compress_type, hash, in_arcname )
    return inval.getinfo( in_arcname )


def test_write_append ():
    out_path = (TEMP_DIR / "test_write_append.jet" )
    if out_path.exists():
        remove( out_path )
    copy( JET_FILEPATH, out_path )
    files = [
        ("./tests/resources/abnormal.csv", ZIP_DEFLATED, "904718c83e715b9dc475ed0a9b7b5dcdb047866e79604b4a996b94df9862771d")
        , ("./tests/resources/valid.dgdata", ZIP_STORED, "0887eecd17663a06c81e9861834ba5f6c62b833741348858244520ef72d28647")
        # Big files.
        , ("./tests/resources/generic.jet", ZIP_STORED, "6a24bdf8d3ffcdacd8b4f7250d468fbf22f7cbf4d0113e01b2c776ca928ec5c7", "big/store.jet")
        , ("./tests/resources/generic.jet", ZIP_DEFLATED, "6a24bdf8d3ffcdacd8b4f7250d468fbf22f7cbf4d0113e01b2c776ca928ec5c7", "big/deflate.jet")
    ]
    shrink_files = set( files )
    with jetfile.JetFile( out_path, mode="a" ) as out_jet:
        # New files.
        for x in files:
            _write( out_jet, *x )
        _read_files( out_jet )
        # Overwrite files.
        overwrite_files = [
            *((*x[0], x[1]) for x in zip(files, ["subdir/lang.csv","subdir/cfg.csv"]))
            # Same exact file for testing out quick encryption.
            , ("./tests/resources/lang.csv", ZIP_DEFLATED, "f88b2b69973ac5ea80c4b9f84f8480507853331fddb1896256d8ddac27f9810b", "lang.csv")
        ]
        overwrite_files.append( overwrite_files[0] )
        for x in overwrite_files:
            _write( out_jet, *x )
        for x in overwrite_files[-2:]:
            zinfo = out_jet.getinfo( x[3] )
            assert zinfo.CRC and zinfo.CRC == zinfo.orig_crc
        shrink_files.update( overwrite_files )
        # No-password files.
        nopwd_files = [
            ("./tests/resources/valid.json", ZIP_STORED, "78564523ecdccbd8868d4468a9140f1c5931c146eaeb8a809fcc943683bbc2f6")
            , overwrite_files[-1]
            , overwrite_files[-2]
        ]
        for x in nopwd_files:
            zinfo = _write( out_jet, *x, pwd=False )
            assert not (zinfo.flag_bits & _MASK_ENCRYPTED)
        for x in nopwd_files[-2:]:
            zinfo = out_jet.getinfo( x[3] )
            assert zinfo.CRC and zinfo.CRC == zinfo.orig_crc
        shrink_files.update( nopwd_files )
        # Shrink test.
        with rebuild(out_jet, out_path.with_name("test_write_append_shrink.jet"), "w") as shrink_jet:
            assert not shrink_jet.testzip()
            for filename, compresstype, filehash, *arcname in shrink_files:
                if arcname:
                    fname = arcname[0]
                else:
                    fname = Path(filename).name
                _read( shrink_jet, compresstype, filehash, fname )
        assert out_jet._fileRefCnt == 1


def _writestr (inval: jetfile.JetFile, arcname: str, compress_type, txt: str, pwd=None):
    inval.writestr( arcname, txt, compress_type, 5
        , allow_duplicate=False, allow_overwrite=True
        , pwd=pwd
    )
    _read( inval, compress_type, sha256(txt.encode("utf8")).hexdigest(), arcname )
    return inval.getinfo( arcname )


def test_writestr_append ():
    out_path = (TEMP_DIR / "test_writestr_append.jet" )
    if out_path.exists():
        remove( out_path )
    copy( JET_FILEPATH, out_path )
    files = [
        ("a", ZIP_STORED, "foo")
        , ("b", ZIP_DEFLATED, "bar")
        , ("subdir/a", ZIP_STORED, "foo")
        , ("subdir/b", ZIP_DEFLATED, "bar")
    ]
    shrink_files = {x[0]:x for x in files}
    with jetfile.JetFile( out_path, mode="a" ) as out_jet:
        # New files.
        for x in files:
            _writestr( out_jet, *x )
        _read_files( out_jet )
        # Overwrite files.
        overwrite_files = [
            ("a", ZIP_STORED, "FOO")
            , ("subdir/lang.csv", ZIP_DEFLATED, "0|bar|BAR")
            # Same exact file for testing out quick encryption.
            , ("subdir/b", ZIP_DEFLATED, "bar")
            , ("a", ZIP_STORED, "FOO")
        ]
        for x in overwrite_files:
            _writestr( out_jet, *x )
        for x in overwrite_files[-2:]:
            zinfo = out_jet.getinfo( x[0] )
            assert zinfo.CRC and zinfo.CRC == zinfo.orig_crc
        shrink_files.update( (x[0],x,) for x in overwrite_files )
        # No-password files.
        nopwd_files = [
            ("subdir/cfg.csv", ZIP_STORED, "foo|foo")
            , ("b", ZIP_DEFLATED, "BAR")
            # Same exact file for testing out quick encryption.
            , ("subdir/a", ZIP_STORED, "foo")
            , ("b", ZIP_DEFLATED, "bar")
        ]
        for x in nopwd_files:
            zinfo = _writestr( out_jet, *x, pwd=False )
            assert not (zinfo.flag_bits & _MASK_ENCRYPTED)
        for x in nopwd_files[-2:]:
            zinfo = out_jet.getinfo( x[0] )
            assert zinfo.CRC and zinfo.CRC == zinfo.orig_crc
        shrink_files.update( (x[0],x,) for x in nopwd_files )
        # Shrink test.
        with rebuild(out_jet, out_path.with_name("test_writestr_append_shrink.jet"), "w") as shrink_jet:
            assert not shrink_jet.testzip()
            for arcname, compresstype, txt in shrink_files.values():
                _read( shrink_jet, compresstype, sha256(txt.encode("utf8")).hexdigest(), arcname )
        assert out_jet._fileRefCnt == 1


def _rebuild (zfile, outpath, outhash, outfile=None, mode=None):
    if outpath.exists():
        try:
            remove( outpath )
        except PermissionError:
            pass
    outfile = outfile or str(outpath)
    with rebuild( zfile, outfile, mode ) as out_zfile:
        _read_files( out_zfile )
        assert out_zfile._fileRefCnt == 1
    if hasattr( outfile, "close" ):
        outfile.close()
    with outpath.open( "rb" ) as fp:
        assert fp_sha256( fp ) == outhash


def test_rebuild ():
    expected_hash = "6a24bdf8d3ffcdacd8b4f7250d468fbf22f7cbf4d0113e01b2c776ca928ec5c7"
    out_path = (TEMP_DIR/"test_rebuild_str.jet")
    with jetfile.JetFile( JET_FILEPATH, mode="r" ) as in_jet:
        # str.
        _rebuild( in_jet, out_path, expected_hash, mode="x" )
        # pathlib.
        out_path = out_path.with_name("test_rebuild_pathlib.jet")
        _rebuild( in_jet, out_path, expected_hash, outfile=out_path, mode="w" )
        # file obj.
        out_path = out_path.with_name("test_rebuild_fp.jet")
        _rebuild( in_jet, out_path, expected_hash, outfile=out_path.open("wb+"), mode="a" )
        # zipfile.
        out_path = out_path.with_name("test_rebuild_zfile.jet")
        _rebuild( in_jet, out_path, expected_hash, outfile=jetfile.JetFile(out_path, mode="w") )
        assert in_jet._fileRefCnt == 1
