import pytest
from typing import Any, Type, Collection, Tuple, Dict
from io import BytesIO
from sys import byteorder as sys_byteorder
import numpy as np
import numpy.typing as npt
from kucingoren.util import ShapedArray
from kucingoren.util.numpy_.dataclasses_ import ABCNumpyDataclass, numpy_dataclass



def _read (dc: ABCNumpyDataclass, validator: Dict[str, Tuple[Any,Type[Any]]]):
    for fname, (vval, vtype) in validator.items():
        fval = getattr( dc, fname )
        if isinstance( fval, Collection ):
            assert np.array_equal( fval, vval )
        else:
            assert fval == vval
        if not isinstance( vtype, type ):
            vtype = type( vtype )
        assert isinstance( fval, vtype )


def _test_factory (
    dc: ABCNumpyDataclass
    , validator: Dict[str, Tuple[Any,Type[Any]]]
    , raw_little: bytes
    , raw_big: bytes
    , *
    , _constructor: str = "frombuffer"
    , _no_object: bool = True
):
    raw_native = raw_little if sys_byteorder == "little" else raw_big
    f = getattr( dc, _constructor )
    if _constructor == "fromfile":
        _f, f = f, (lambda buf,*a,**kwa: _f(BytesIO(buf),*a,**kwa))
    for out_dc in [
        f(raw_little, byteorder="<")
        , f(raw_big, byteorder=">")
    ]:
        _read( out_dc, validator )
        if _no_object:
            assert not out_dc.dtype.hasobject
    for bo, eb in (("<",raw_little,), (">",raw_big),):
        assert f(eb, byteorder=bo).tobytes() == raw_native
        assert f(raw_little, byteorder="<").tobytes(byteorder=bo) == eb
        assert f(raw_big, byteorder=">").tobytes(byteorder=bo) == eb


def _test (*args, **kwargs):
    _test_factory(*args, _constructor="frombuffer", **kwargs)
    _test_factory(*args, _constructor="fromfile", **kwargs)


EightLengthUnicodeType = np.dtype( "U8" )
SAMPLE_UI16 = np.uint16( 5 )
SAMPLE_F64 = np.float64( 3.14 )
SAMPLE_SU8 = np.asarray( "ayylmao", EightLengthUnicodeType )
SAMPLE_N = np.asarray( 14.3, None )
SAMPLE_BOOL = np.bool_( True )
BASIC_RAW_BYTES_LITTLE = b''.join(
    x.newbyteorder("<").tobytes()
    for x
    in [SAMPLE_UI16, SAMPLE_F64, SAMPLE_SU8, SAMPLE_N, SAMPLE_BOOL,]
)
BASIC_RAW_BYTES_BIG = b''.join(
    x.newbyteorder(">").tobytes()
    for x
    in [
        SAMPLE_UI16
        , SAMPLE_F64
        , np.asarray( SAMPLE_SU8, EightLengthUnicodeType.newbyteorder(">") )
        , np.asarray( SAMPLE_N, SAMPLE_N.dtype.newbyteorder(">") )
        , SAMPLE_BOOL
    ]
)
BASIC_FIELDS = {
    "numpy_uint16": (SAMPLE_UI16, np.uint16)
    , "builtin_float": (SAMPLE_F64, float)
    , "numpy_unicodestr": (SAMPLE_SU8, np.ndarray)
    , "numpy_default": (SAMPLE_N, np.obj2sctype(None))
    , "builtin_bool": (SAMPLE_BOOL, bool)
}
@numpy_dataclass
class Basic (ABCNumpyDataclass):
    numpy_uint16: np.uint16
    builtin_float: float
    numpy_unicodestr: npt.NDArray[EightLengthUnicodeType]
    # This is also allowed but type-linter would complains for it.
    # numpy_unicodestr: EightLengthUnicodeType
    numpy_default: None
    builtin_bool: bool


def test_scalars ():
    _test( Basic, BASIC_FIELDS, BASIC_RAW_BYTES_LITTLE, BASIC_RAW_BYTES_BIG )


SAMPLE_FIXED_NP = np.zeros( 5, "b" )
SAMPLE_FIXED_TUPLE = (1,) * 3
SAMPLE_FIXED_LIST = [2,] * 2
ARRFIXED_RAW_BYTES_LITTLE = (
    BASIC_RAW_BYTES_LITTLE
    + np.asarray( SAMPLE_FIXED_NP, SAMPLE_FIXED_NP.dtype.newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_FIXED_TUPLE, np.dtype(int).newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_FIXED_LIST, np.dtype(int).newbyteorder("<") ).tobytes()
)
ARRFIXED_RAW_BYTES_BIG = (
    BASIC_RAW_BYTES_BIG
    + np.asarray( SAMPLE_FIXED_NP, SAMPLE_FIXED_NP.dtype.newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_FIXED_TUPLE, np.dtype(int).newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_FIXED_LIST, np.dtype(int).newbyteorder(">") ).tobytes()
)
ARRFIXED_FIELDS = BASIC_FIELDS.copy()
ARRFIXED_FIELDS.update({
    "ndshaped": (SAMPLE_FIXED_NP, np.ndarray)
    , "builtin_tuple": (SAMPLE_FIXED_TUPLE, tuple)
    , "builtin_list": (SAMPLE_FIXED_LIST, list)
})
@numpy_dataclass
class FixedArrays (Basic):
    ndshaped: ShapedArray[npt.NDArray, SAMPLE_FIXED_NP.shape, SAMPLE_FIXED_NP.dtype]
    builtin_tuple: ShapedArray[tuple, len(SAMPLE_FIXED_TUPLE), int]
    builtin_list: ShapedArray[list, len(SAMPLE_FIXED_LIST), int]


def test_arrays_fixed ():
    _test( FixedArrays, ARRFIXED_FIELDS, ARRFIXED_RAW_BYTES_LITTLE, ARRFIXED_RAW_BYTES_BIG )


SAMPLE_DYNAMIC_NP = np.arange( SAMPLE_UI16 )
SAMPLE_DYNAMIC_LIST = SAMPLE_DYNAMIC_NP.tolist()
SAMPLE_DYNAMIC_TUPLE = tuple( SAMPLE_DYNAMIC_LIST )
SAMPLE_UI32 = np.uint32( len(SAMPLE_DYNAMIC_NP) )
ARRDYNAMIC_RAW_BYTES_LITTLE = (
    ARRFIXED_RAW_BYTES_LITTLE
    + np.asarray( SAMPLE_DYNAMIC_NP, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_TUPLE, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_LIST, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
    + SAMPLE_UI32.newbyteorder("<").tobytes()
    + np.asarray( SAMPLE_DYNAMIC_NP, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_TUPLE, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_LIST, SAMPLE_DYNAMIC_NP.dtype.newbyteorder("<") ).tobytes()
)
ARRDYNAMIC_RAW_BYTES_BIG = (
    ARRFIXED_RAW_BYTES_BIG
    + np.asarray( SAMPLE_DYNAMIC_NP, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_TUPLE, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_LIST, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
    + SAMPLE_UI32.newbyteorder(">").tobytes()
    + np.asarray( SAMPLE_DYNAMIC_NP, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_TUPLE, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
    + np.asarray( SAMPLE_DYNAMIC_LIST, SAMPLE_DYNAMIC_NP.dtype.newbyteorder(">") ).tobytes()
)
ARRDYNAMIC_FIELDS = ARRFIXED_FIELDS.copy()
ARRDYNAMIC_FIELDS.update({
    "ndshaped_super": (SAMPLE_DYNAMIC_NP, np.ndarray)
    , "tuple_super": (SAMPLE_DYNAMIC_TUPLE, tuple)
    , "list_super": (SAMPLE_DYNAMIC_LIST, list)
    , "this_len": (SAMPLE_UI32, np.uint32)
    , "ndshaped_this": (SAMPLE_DYNAMIC_NP, np.ndarray)
    , "tuple_this": (SAMPLE_DYNAMIC_TUPLE, tuple)
    , "list_this": (SAMPLE_DYNAMIC_LIST, list)
})
@numpy_dataclass
class DynamicArrays (FixedArrays):
    ndshaped_super: ShapedArray[npt.NDArray, "numpy_uint16", SAMPLE_DYNAMIC_NP.dtype]
    tuple_super: ShapedArray[tuple, "numpy_uint16", int]
    list_super: ShapedArray[list, "numpy_uint16", int]
    this_len: np.uint32
    ndshaped_this: ShapedArray[npt.NDArray, "this_len", SAMPLE_DYNAMIC_NP.dtype]
    tuple_this: ShapedArray[tuple, "this_len", int]
    list_this: ShapedArray[list, "this_len", int]


def test_arrays_dynamic ():
    _test( DynamicArrays, ARRDYNAMIC_FIELDS, ARRDYNAMIC_RAW_BYTES_LITTLE, ARRDYNAMIC_RAW_BYTES_BIG )


SAMPLE_DCBASIC = Basic.frombuffer( BASIC_RAW_BYTES_LITTLE, byteorder="<" )


def test_npdataclass ():
    SAMPLE_DCBASIC_TUPLE = tuple(
        SAMPLE_DCBASIC
        for _ in range(1)
    )
    SAMPLE_DCBASIC_LIST = list( SAMPLE_DCBASIC_TUPLE )
    SAMPLE_DCBASIC_NP = np.asarray( SAMPLE_DCBASIC_LIST, dtype=object )
    SAMPLE_DCBASIC_TUPLE_SECOND = tuple(
        SAMPLE_DCBASIC
        for _ in range(SAMPLE_UI32)
    )
    SAMPLE_DCBASIC_LIST_SECOND = list( SAMPLE_DCBASIC_TUPLE_SECOND )
    SAMPLE_DCBASIC_NP_SECOND = np.asarray( SAMPLE_DCBASIC_LIST_SECOND, dtype=object )
    SAMPLE_DCDYN = DynamicArrays.frombuffer( ARRDYNAMIC_RAW_BYTES_LITTLE, byteorder="<" )
    SAMPLE_DCDYN_TUPLE = tuple(
        SAMPLE_DCDYN
        for _ in range(1)
    )
    SAMPLE_DCDYN_LIST = list( SAMPLE_DCDYN_TUPLE )
    SAMPLE_DCDYN_NP = np.asarray( SAMPLE_DCDYN_LIST, dtype=object )
    SAMPLE_DCDYN_TUPLE_SECOND = tuple(
        SAMPLE_DCDYN
        for _ in range(SAMPLE_UI32)
    )
    SAMPLE_DCDYN_LIST_SECOND = list( SAMPLE_DCDYN_TUPLE_SECOND )
    SAMPLE_DCDYN_NP_SECOND = np.asarray( SAMPLE_DCDYN_LIST_SECOND, dtype=object )
    DC_RAW_BYTES_LITTLE = (
        ARRDYNAMIC_RAW_BYTES_LITTLE
        + SAMPLE_DCBASIC.tobytes("<")
        + b''.join( x.tobytes("<") for x in SAMPLE_DCBASIC_TUPLE ) * 3
        + b''.join( x.tobytes("<") for x in SAMPLE_DCBASIC_TUPLE_SECOND ) * 3
        + SAMPLE_UI32.newbyteorder("<").tobytes()
        + b''.join( x.tobytes("<") for x in SAMPLE_DCBASIC_TUPLE_SECOND ) * 3
        + SAMPLE_DCDYN.tobytes("<")
        + b''.join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLE ) * 3
        + b''.join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLE_SECOND ) * 3
        + b''.join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLE_SECOND ) * 3
    )
    DC_RAW_BYTES_BIG = (
        ARRDYNAMIC_RAW_BYTES_BIG
        + SAMPLE_DCBASIC.tobytes(">")
        + b''.join( x.tobytes(">") for x in SAMPLE_DCBASIC_TUPLE ) * 3
        + b''.join( x.tobytes(">") for x in SAMPLE_DCBASIC_TUPLE_SECOND ) * 3
        + SAMPLE_UI32.newbyteorder(">").tobytes()
        + b''.join( x.tobytes(">") for x in SAMPLE_DCBASIC_TUPLE_SECOND ) * 3
        + SAMPLE_DCDYN.tobytes(">")
        + b''.join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLE ) * 3
        + b''.join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLE_SECOND ) * 3
        + b''.join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLE_SECOND ) * 3
    )
    DC_FIELDS = ARRDYNAMIC_FIELDS.copy()
    DC_FIELDS.update({
        # basic dataclass.
        "dcbasic": (SAMPLE_DCBASIC, Basic)
        , "dcbasic_ndshaped": (SAMPLE_DCBASIC_NP, np.ndarray)
        , "dcbasic_tuple": (SAMPLE_DCBASIC_TUPLE, tuple)
        , "dcbasic_list": (SAMPLE_DCBASIC_LIST, list)
        , "dcbasic_ndshaped_super": (SAMPLE_DCBASIC_NP_SECOND, np.ndarray)
        , "dcbasic_tuple_super": (SAMPLE_DCBASIC_TUPLE_SECOND, tuple)
        , "dcbasic_list_super": (SAMPLE_DCBASIC_LIST_SECOND, list)
        , "dc_this_len": (SAMPLE_UI32, np.uint32)
        , "dcbasic_ndshaped_this": (SAMPLE_DCBASIC_NP_SECOND, np.ndarray)
        , "dcbasic_tuple_this": (SAMPLE_DCBASIC_TUPLE_SECOND, tuple)
        , "dcbasic_list_this": (SAMPLE_DCBASIC_LIST_SECOND, list)
        # dataclass which has dynamic field.
        , "dcdyn": (SAMPLE_DCDYN, DynamicArrays)
        , "dcdyn_ndshaped": (SAMPLE_DCDYN_NP, np.ndarray)
        , "dcdyn_tuple": (SAMPLE_DCDYN_TUPLE, tuple)
        , "dcdyn_list": (SAMPLE_DCDYN_LIST, list)
        , "dcdyn_ndshaped_super": (SAMPLE_DCDYN_NP_SECOND, np.ndarray)
        , "dcdyn_tuple_super": (SAMPLE_DCDYN_TUPLE_SECOND, tuple)
        , "dcdyn_list_super": (SAMPLE_DCDYN_LIST_SECOND, list)
        , "dcdyn_ndshaped_this": (SAMPLE_DCDYN_NP_SECOND, np.ndarray)
        , "dcdyn_tuple_this": (SAMPLE_DCDYN_TUPLE_SECOND, tuple)
        , "dcdyn_list_this": (SAMPLE_DCDYN_LIST_SECOND, list)
    })
    @numpy_dataclass
    class DC (DynamicArrays):
        # basic dataclass.
        dcbasic: Basic
        dcbasic_ndshaped: ShapedArray[npt.NDArray, SAMPLE_DCBASIC_NP.shape, Basic]
        dcbasic_tuple: ShapedArray[tuple, len(SAMPLE_DCBASIC_TUPLE), Basic]
        dcbasic_list: ShapedArray[list, len(SAMPLE_DCBASIC_LIST), Basic]
        dcbasic_ndshaped_super: ShapedArray[npt.NDArray, "numpy_uint16", Basic]
        dcbasic_tuple_super: ShapedArray[tuple, "numpy_uint16", Basic]
        dcbasic_list_super: ShapedArray[list, "numpy_uint16", Basic]
        dc_this_len: np.uint32
        dcbasic_ndshaped_this: ShapedArray[npt.NDArray, "dc_this_len", Basic]
        dcbasic_tuple_this: ShapedArray[tuple, "dc_this_len", Basic]
        dcbasic_list_this: ShapedArray[list, "dc_this_len", Basic]
        # dataclass which has dynamic field.
        dcdyn: DynamicArrays
        dcdyn_ndshaped: ShapedArray[npt.NDArray, SAMPLE_DCDYN_NP.shape, DynamicArrays]
        dcdyn_tuple: ShapedArray[tuple, len(SAMPLE_DCDYN_TUPLE), DynamicArrays]
        dcdyn_list: ShapedArray[list, len(SAMPLE_DCDYN_LIST), DynamicArrays]
        dcdyn_ndshaped_super: ShapedArray[npt.NDArray, "numpy_uint16", DynamicArrays]
        dcdyn_tuple_super: ShapedArray[tuple, "numpy_uint16", DynamicArrays]
        dcdyn_list_super: ShapedArray[list, "numpy_uint16", DynamicArrays]
        dc_this_len: np.uint32
        dcdyn_ndshaped_this: ShapedArray[npt.NDArray, "dc_this_len", DynamicArrays]
        dcdyn_tuple_this: ShapedArray[tuple, "dc_this_len", DynamicArrays]
        dcdyn_list_this: ShapedArray[list, "dc_this_len", DynamicArrays]
    _test( DC, DC_FIELDS, DC_RAW_BYTES_LITTLE, DC_RAW_BYTES_BIG )


SAMPLE_EMPTY_NP = np.array( [], dtype=int )
SAMPLE_EMPTY_LIST = SAMPLE_EMPTY_NP.tolist()
SAMPLE_EMPTY_TUPLE = tuple( SAMPLE_EMPTY_NP )
SAMPLE_DCBASIC_EMPTY_NP = np.array( [], dtype=Basic )
SAMPLE_DCBASIC_EMPTY_LIST = SAMPLE_DCBASIC_EMPTY_NP.tolist()
SAMPLE_DCBASIC_EMPTY_TUPLE = tuple( SAMPLE_DCBASIC_EMPTY_NP )
SAMPLE_DCDYN_EMPTY_NP = np.array( [], dtype=DynamicArrays )
SAMPLE_DCDYN_EMPTY_LIST = SAMPLE_DCDYN_EMPTY_NP.tolist()
SAMPLE_DCDYN_EMPTY_TUPLE = tuple( SAMPLE_DCDYN_EMPTY_NP )
SAMPLE_UI32_ZERO = np.uint32( 0 )
ARREMPTY_RAW_BYTES_LITTLE = (
    SAMPLE_UI32_ZERO.newbyteorder("<").tobytes()
)
ARREMPTY_RAW_BYTES_BIG = (
    SAMPLE_UI32_ZERO.newbyteorder(">").tobytes()
)
ARREMPTY_FIELDS = {
    "ndshaped_empty": (SAMPLE_EMPTY_NP, np.ndarray)
    , "tuple_empty": (SAMPLE_EMPTY_TUPLE, tuple)
    , "list_empty": (SAMPLE_EMPTY_LIST, list)
    , "dcbasic_ndshaped_empty": (SAMPLE_DCBASIC_EMPTY_NP, np.ndarray)
    , "dcbasic_tuple_empty": (SAMPLE_DCBASIC_EMPTY_TUPLE, tuple)
    , "dcbasic_list_empty": (SAMPLE_DCBASIC_EMPTY_LIST, list)
    , "dcdyn_ndshaped_empty": (SAMPLE_DCDYN_EMPTY_NP, np.ndarray)
    , "dcdyn_tuple_empty": (SAMPLE_DCDYN_EMPTY_TUPLE, tuple)
    , "dcdyn_list_empty": (SAMPLE_DCDYN_EMPTY_LIST, list)
    , "zero_len": (SAMPLE_UI32_ZERO, np.uint32)
    , "ndshaped_zero": (SAMPLE_EMPTY_NP, np.ndarray)
    , "tuple_zero": (SAMPLE_EMPTY_TUPLE, tuple)
    , "list_zero": (SAMPLE_EMPTY_LIST, list)
    , "dcbasic_ndshaped_zero": (SAMPLE_DCBASIC_EMPTY_NP, np.ndarray)
    , "dcbasic_tuple_zero": (SAMPLE_DCBASIC_EMPTY_TUPLE, tuple)
    , "dcbasic_list_zero": (SAMPLE_DCBASIC_EMPTY_LIST, list)
    , "dcdyn_ndshaped_zero": (SAMPLE_DCDYN_EMPTY_NP, np.ndarray)
    , "dcdyn_tuple_zero": (SAMPLE_DCDYN_EMPTY_TUPLE, tuple)
    , "dcdyn_list_zero": (SAMPLE_DCDYN_EMPTY_LIST, list)
}
@numpy_dataclass
class EmptyArrays (ABCNumpyDataclass):
    # Fixed.
    ndshaped_empty: ShapedArray[npt.NDArray, 0, SAMPLE_EMPTY_NP.dtype]
    tuple_empty: ShapedArray[tuple, 0, int]
    list_empty: ShapedArray[list, 0, int]
    dcbasic_ndshaped_empty: ShapedArray[npt.NDArray, 0, Basic]
    dcbasic_tuple_empty: ShapedArray[tuple, 0, Basic]
    dcbasic_list_empty: ShapedArray[list, 0, Basic]
    dcdyn_ndshaped_empty: ShapedArray[npt.NDArray, 0, DynamicArrays]
    dcdyn_tuple_empty: ShapedArray[tuple, 0, DynamicArrays]
    dcdyn_list_empty: ShapedArray[list, 0, DynamicArrays]
    # Dynamic.
    zero_len: np.uint32
    ndshaped_zero: ShapedArray[npt.NDArray, "zero_len", SAMPLE_EMPTY_NP.dtype]
    tuple_zero: ShapedArray[tuple, "zero_len", int]
    list_zero: ShapedArray[list, "zero_len", int]
    dcdyn_ndshaped_zero: ShapedArray[npt.NDArray, "zero_len", DynamicArrays]
    dcdyn_tuple_zero: ShapedArray[tuple, "zero_len", DynamicArrays]
    dcdyn_list_zero: ShapedArray[list, "zero_len", DynamicArrays]
    dcbasic_ndshaped_zero: ShapedArray[npt.NDArray, "zero_len", Basic]
    dcbasic_tuple_zero: ShapedArray[tuple, "zero_len", Basic]
    dcbasic_list_zero: ShapedArray[list, "zero_len", Basic]


def test_arrays_empty ():
    _test(
        EmptyArrays, ARREMPTY_FIELDS, ARREMPTY_RAW_BYTES_LITTLE, ARREMPTY_RAW_BYTES_BIG
        , _no_object=False
    )


def test_arrays_tupleshaped ():
    SAMPLE_SHAPE = (3,2,)
    SAMPLE_TUPLESHAPED_NP = np.arange(6).reshape(SAMPLE_SHAPE)
    SAMPLE_TUPLESHAPED_LIST = SAMPLE_TUPLESHAPED_NP.tolist()
    SAMPLE_TUPLESHAPED_TUPLE = tuple( SAMPLE_TUPLESHAPED_LIST )
    SAMPLE_DCDYN = DynamicArrays.frombuffer( ARRDYNAMIC_RAW_BYTES_LITTLE, byteorder="<" )
    SAMPLE_DCDYN_TUPLESHAPED_LIST = [ (SAMPLE_DCDYN,)*2 for _ in range(3) ]
    SAMPLE_DCDYN_TUPLESHAPED_TUPLE = tuple( SAMPLE_DCDYN_TUPLESHAPED_LIST )
    SAMPLE_DCDYN_TUPLESHAPED_NP = np.array( SAMPLE_DCDYN_TUPLESHAPED_LIST, dtype=object )
    ARRTUPLESHAPED_RAW_BYTES_LITTLE = (
        np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + np.asarray(SAMPLE_SHAPE, dtype="<u4").tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder("<")).tobytes()
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes("<") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
    )
    ARRTUPLESHAPED_RAW_BYTES_BIG = (
        np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + np.asarray(SAMPLE_SHAPE, dtype=">u4").tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + np.asarray(SAMPLE_TUPLESHAPED_NP, SAMPLE_TUPLESHAPED_NP.dtype.newbyteorder(">")).tobytes()
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
        + b"".join( x.tobytes(">") for x in SAMPLE_DCDYN_TUPLESHAPED_NP.flat )
    )
    ARRTUPLESHAPED_FIELDS = {
        "ndshaped": (SAMPLE_TUPLESHAPED_NP, np.ndarray)
        , "tuple": (SAMPLE_TUPLESHAPED_TUPLE, tuple)
        , "list": (SAMPLE_TUPLESHAPED_LIST, list)
        , "dc_ndshaped": (SAMPLE_DCDYN_TUPLESHAPED_NP, np.ndarray)
        , "dc_tuple": (SAMPLE_DCDYN_TUPLESHAPED_TUPLE, tuple)
        , "dc_list": (SAMPLE_DCDYN_TUPLESHAPED_LIST, list)
        , "tupleshaped": (SAMPLE_SHAPE, tuple)
        , "ndshaped_tupleshaped": (SAMPLE_TUPLESHAPED_NP, np.ndarray)
        , "tuple_tupleshaped": (SAMPLE_TUPLESHAPED_TUPLE, tuple)
        , "list_tupleshaped": (SAMPLE_TUPLESHAPED_LIST, list)
        , "dc_ndshaped_tupleshaped": (SAMPLE_DCDYN_TUPLESHAPED_NP, np.ndarray)
        , "dc_tuple_tupleshaped": (SAMPLE_DCDYN_TUPLESHAPED_TUPLE, tuple)
        , "dc_list_tupleshaped": (SAMPLE_DCDYN_TUPLESHAPED_LIST, list)
    }
    @numpy_dataclass
    class TupleShapedArrays (ABCNumpyDataclass):
        # Fixed.
        ndshaped: ShapedArray[npt.NDArray, SAMPLE_SHAPE, SAMPLE_TUPLESHAPED_NP.dtype]
        tuple: ShapedArray[tuple, SAMPLE_SHAPE, int]
        list: ShapedArray[list, SAMPLE_SHAPE, int]
        dc_ndshaped: ShapedArray[npt.NDArray, SAMPLE_SHAPE, DynamicArrays]
        dc_tuple: ShapedArray[tuple, SAMPLE_SHAPE, DynamicArrays]
        dc_list: ShapedArray[list, SAMPLE_SHAPE, DynamicArrays]
        # Dynamic.
        tupleshaped: ShapedArray[tuple, len(SAMPLE_SHAPE), np.uint]
        ndshaped_tupleshaped: ShapedArray[npt.NDArray, "tupleshaped", SAMPLE_TUPLESHAPED_NP.dtype]
        tuple_tupleshaped: ShapedArray[tuple, "tupleshaped", int]
        list_tupleshaped: ShapedArray[list, "tupleshaped", int]
        dc_ndshaped_tupleshaped: ShapedArray[npt.NDArray, "tupleshaped", DynamicArrays]
        dc_tuple_tupleshaped: ShapedArray[tuple, "tupleshaped", DynamicArrays]
        dc_list_tupleshaped: ShapedArray[list, "tupleshaped", DynamicArrays]
    _test( TupleShapedArrays, ARRTUPLESHAPED_FIELDS, ARRTUPLESHAPED_RAW_BYTES_LITTLE, ARRTUPLESHAPED_RAW_BYTES_BIG )
