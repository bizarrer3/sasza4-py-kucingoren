import pytest
import typing
import uuid
import datetime as dt
from warnings import warn
from io import StringIO, BytesIO
from enum import auto, Enum
from dataclasses import is_dataclass, asdict, dataclass
import numpy as np
from kucingoren.util.json_ import get_json_module
json = get_json_module()
from kucingoren.util.serializer import serialize_f2p, LeastTwoPrecisionDigitsFloat



@dataclass
class DataclassSample:
    x: int = 1
    y: float = 2
    z: bool = True


class EnumSample (Enum):
    A = 1
    B = 2
    C = auto()
    D = [1,3,2,]


SAMPLES = [
    1
    , 1.1
    , np.uint(1)
    , np.float64(1.1)
    , np.zeros((2,3,4,))
    , {
        "foo": True
        , "bar": False
    }
    , dt.datetime.utcnow()
    , dt.datetime.utcnow().date()
    , dt.time()
    , uuid.uuid4()
    , DataclassSample()
    , DataclassSample(z=False)
    , EnumSample.A
    , EnumSample.B
    , EnumSample.C
    , EnumSample.D
    , None
    , serialize_f2p(1.2)
]


def _compare_samples (inval: typing.Iterable[typing.Any]):
    for i,x in enumerate(inval):
        expected = SAMPLES[i]

        if is_dataclass( expected ):
            assert x == asdict( expected )
            continue
        if isinstance( expected, (dt.datetime,dt.date,dt.time) ):
            assert str(x) == expected.isoformat()
            continue
        if isinstance( expected, Enum ):
            assert x == expected.value
            continue
        if isinstance( x, float ):
            assert x == float(expected)
            continue
        if isinstance( expected, np.ndarray ):
            assert np.array_equal( x, expected )
            continue
        if not isinstance( x, type(expected) ):
            assert str(x) == str(expected)
            continue

        assert x == expected


def _default (obj):
    if isinstance( obj, LeastTwoPrecisionDigitsFloat ):
        return str( obj )
    raise TypeError
warn( f"Using {json.__name__} module" )


def test_str ():
    _compare_samples( json.loads(json.dumps(SAMPLES, default=_default)) )


def test_fp ():
    with StringIO() as fp:
        json.dump( SAMPLES, fp, default=_default )
        fp.seek( 0 )
        _compare_samples( json.load(fp) )
    with BytesIO() as fp:
        json.dump( SAMPLES, fp, default=_default )
        fp.seek( 0 )
        _compare_samples( json.load(fp) )


def test_invalid ():
    with pytest.raises( json.JSONDecodeError ):
        json.loads( "ayayayay [PILLAR MEN AWAKEN]" )
    with pytest.raises( json.JSONEncodeError ):
        json.dumps( repr )
